import React from 'react';

// import { getRealPrice } from "../helpers";
import defaultImage from '../img/hero.jpg';

const GameCard = ({ gameData: { cover_art, price, name }, onClick }) => {
  return (
    <div className="game-card" onClick={() => onClick.call()}>
      <div className="game-info">
        <h2 className="game-title">{name}</h2>
        <p className="game-desc"></p>
      </div>
      <div className="game-price">
        <h3>PRICE: </h3>
        <h3>{price}$</h3>
      </div>
      <img
        src={cover_art?.url || defaultImage}
        alt={name}
        className="bg-image"
      />
    </div>
  );
};

export default GameCard;
