import React from 'react';

const BackgroundAnimation = ({ children, onClick }) => {
  return (
    <div
      className="animated-bg"
      onClick={() => (onClick ? onClick.call() : null)}
    >
      <div className="list-item-bg">
        <div className="tint-color"></div>
        <div className="secondary-color"></div>
      </div>
      {children}
    </div>
  );
};

export default BackgroundAnimation;
