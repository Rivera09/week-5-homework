import React, { memo } from 'react';

const Comment = ({ userName, comment }) => {
  return (
    <div className="comment">
      <h3>{userName}</h3>
      <p>{comment}</p>
    </div>
  );
};

export default memo(Comment);
