import React from 'react';

const Header = ({ title }) => {
  return (
    <header className="main-header">
      {/* <Navbar /> */}
      <h1>{title}</h1>
    </header>
  );
};

Header.defaultProps = {
  title: 'Retro game store',
};

export default Header;
