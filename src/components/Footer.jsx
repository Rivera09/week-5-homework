import React from 'react';

const Footer = () => {
  return (
    <footer>
      <h4>
        &copy; {new Date().getFullYear()} Retro game store. All rights reserved
      </h4>
    </footer>
  );
};

export default Footer;
