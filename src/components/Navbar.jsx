import React, { useContext, useState, useEffect } from 'react';

import logo from '../img/logo.png';
import { HOME_PAGE, GAMES_LIST, LOGIN_PAGE } from '../consts';
import { RouterContext } from '../routerContext';

const Navbar = () => {
  const { setCurrentPage, authData, setAuthData } = useContext(RouterContext);
  const [active, setActive] = useState(false);
  const [dark, setDark] = useState(false);

  useEffect(() => {
    window.onscroll = () => {
      const isTop = window.scrollY === 0;
      if (isTop) setDark(() => false);
      else if (!isTop && !dark) setDark(() => true);
    };
  }, []);

  return (
    <nav className={`main-nav ${active ? 'active' : ''} ${dark ? 'dark' : ''}`}>
      <img src={logo} alt="logo" onClick={() => setCurrentPage(HOME_PAGE)} />
      <ul className={active ? 'active' : ''}>
        <li onClick={() => setCurrentPage(HOME_PAGE)}>Home</li>
        <li onClick={() => setCurrentPage(GAMES_LIST)}>Games</li>
        <li
          onClick={() => {
            if (authData.isAuthed) setAuthData();
            else setCurrentPage(LOGIN_PAGE);
          }}
        >
          {authData.isAuthed ? 'Logout' : 'Login'}
        </li>
      </ul>
      <div
        className={`burger ${active ? 'active' : ''}`}
        onClick={() => setActive((prevValue) => !prevValue)}
      >
        <div className="line1"></div>
        <div className="line2"></div>
        <div className="line3"></div>
      </div>
    </nav>
  );
};

export default Navbar;
