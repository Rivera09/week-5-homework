import React, { createContext } from 'react';

export const RouterContext = createContext();

export const RouterProvider = ({ children, value }) => {
  return (
    <RouterContext.Provider value={value}>{children}</RouterContext.Provider>
  );
};
