import React, { useState, useEffect } from 'react';

import { RouterProvider } from './routerContext';
import { HOME_PAGE, GAMES_LIST, GAME_DETAILS } from './consts';
import { Home, GameDetails, GamesList, Login } from './pages';
import { usePrevPage, useAuth } from './hooks';
import { Footer, Navbar } from './components';
import Connection from './apiConection';

import './sass/style.scss';

const apiConnection = new Connection();

function App() {
  const [currentPage, setCurrentPage] = useState(HOME_PAGE);
  const [gameData, setGameData] = useState({});
  const [prevPage, setPrevPage] = usePrevPage();
  const [authData, setAuthData] = useAuth();

  useEffect(() => {
    setPrevPage(currentPage);
  }, [currentPage, setPrevPage]);

  return (
    <>
      <RouterProvider
        value={{
          setCurrentPage,
          setGameData,
          apiConnection,
          prevPage,
          authData,
          setAuthData,
        }}
      >
        <Navbar />
        {currentPage === HOME_PAGE ? (
          <Home />
        ) : currentPage === GAMES_LIST ? (
          <GamesList />
        ) : currentPage === GAME_DETAILS ? (
          <GameDetails gameData={gameData} />
        ) : (
          <Login />
        )}
        {/* <GamesList /> */}
      </RouterProvider>
      <Footer />
    </>
  );
}

export default App;
