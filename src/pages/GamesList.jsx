import React, { useState, useEffect, useContext } from 'react';

import { Header, GameListItem, Spinner } from '../components';
import { RouterContext } from '../routerContext';
import { GAMES_URL, GAME_DETAILS, LIMIT } from '../consts';

const GamesList = () => {
  const { apiConnection, setGameData, setCurrentPage } = useContext(
    RouterContext
  );
  const [gamesList, setGamesList] = useState([]);
  const [currentPagination, setCurrentPagination] = useState(0);
  const [showLoadMoreBtn, setShowLoadMoreBtn] = useState(true);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    const fetchGames = async () => {
      const { resBody, error } = await apiConnection.getData(
        GAMES_URL.replace(':start', currentPagination)
      );
      if (error) return console.log('Error');
      setGamesList((prevGames) => [...prevGames, ...resBody]);
      if (!resBody.length || resBody.length < LIMIT) setShowLoadMoreBtn(false);
      setLoading(false);
      // console.log(resBody);
    };
    fetchGames();
  }, [currentPagination, apiConnection]);

  if (loading) return <Spinner />;
  return (
    <>
      <Header title="List of games" />
      <div className="games-list-container">
        <div className="games-list">
          {gamesList.map((gameItem) => (
            <GameListItem
              gameDetails={gameItem}
              key={gameItem.id}
              onClick={() => {
                setCurrentPage(GAME_DETAILS);
                setGameData(gameItem);
              }}
            />
          ))}
        </div>
        {showLoadMoreBtn ? (
          <div
            className="games-list-item"
            onClick={() => setCurrentPagination((prevPage) => prevPage + LIMIT)}
          >
            <div className="list-item-bg">
              <div className="tint-color"></div>
              <div className="secondary-color"></div>
            </div>
            <button>load more</button>
          </div>
        ) : null}
      </div>
    </>
  );
};

export default GamesList;
