import React, { useState, useContext } from 'react';

import { Header, BackgroundAnimation } from '../components';
import { RouterContext } from '../routerContext';
import { LOGIN_API_URL } from '../consts';

const Login = () => {
  const [formData, setFormData] = useState({
    identifier: '',
    password: '',
  });
  const { apiConnection, setCurrentPage, prevPage, setAuthData } = useContext(
    RouterContext
  );

  const handleFormData = (e) => {
    setFormData((prevData) => ({
      ...prevData,
      [e.target.name]: e.target.value,
    }));
  };

  const onSubmit = async () => {
    const { resBody, error } = await apiConnection.postData(
      LOGIN_API_URL,
      formData
    );
    if (error) return console.log('error');
    // console.log(resBody);
    setAuthData(resBody);
    setCurrentPage(prevPage);
  };

  const onCancel = () => {
    setCurrentPage(prevPage);
  };
  const { identifier, password } = formData;

  return (
    <>
      <Header title="Login" />
      <div className="login-container">
        <form className="login-form">
          <div className="form-input">
            <label>User</label>
            <input
              type="text"
              name="identifier"
              autoComplete="off"
              value={identifier}
              onChange={handleFormData}
            />
          </div>
          <div className="form-input">
            <label>Password</label>
            <input
              type="password"
              name="password"
              value={password}
              onChange={handleFormData}
            />
          </div>
          <div className="login-btns-container">
            <BackgroundAnimation onClick={onCancel}>
              <button type="submit">Cancel</button>
            </BackgroundAnimation>
            <BackgroundAnimation onClick={onSubmit}>
              <button type="submit">Log in</button>
            </BackgroundAnimation>
          </div>
        </form>
      </div>
    </>
  );
};

export default Login;
