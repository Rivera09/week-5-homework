import React, {
  useContext,
  useRef,
  useState,
  useCallback,
  useEffect,
} from 'react';

import { Comment } from '../components';
import BackgroundAnimation from '../components/BackgroundAnimation';
import { RouterContext } from '../routerContext';
import { POST_COMMENT_URL } from '../consts';

import heroImage from '../img/hero.jpg';

const GameDetails = ({
  gameData: {
    cover_art,
    name,
    description,
    price,
    genre,
    publishers,
    comments,
    id,
  },
}) => {
  const [gameComments, setGameComments] = useState([]);
  const { authData, apiConnection } = useContext(RouterContext);
  const inputRef = useRef();

  const updateComments = useCallback((newComment) => {
    if (newComment instanceof Array)
      return setGameComments((prevComments) => [
        ...prevComments,
        ...newComment,
      ]);
    setGameComments((prevComments) => [...prevComments, newComment]);
  }, []);

  useEffect(() => {
    updateComments(comments);
  }, [updateComments, comments]);

  const onSubmit = async (e) => {
    e.preventDefault();
    const body = inputRef.current.value;
    const headers = {
      Authorization: `Bearer ${authData.jwt}`,
    };
    if (!body) return;
    const { resBody, error } = await apiConnection.postData(
      POST_COMMENT_URL.replace(':gameId', id),
      {
        body,
      },
      headers
    );
    if (error) return console.log('error');
    updateComments({
      body: resBody.body,
      id: resBody.id,
      trainee: resBody.trainee,
      user: resBody.user,
    });
    inputRef.current.value = '';
  };
  return (
    <div className="game-details-container">
      <div className="hero">
        <img src={cover_art?.url || heroImage} alt="gta v" />
      </div>
      <div className="game-details">
        <div>
          <h2>
            {name} - {publishers[0]?.name}
          </h2>
          <p>{genre?.name}</p>
        </div>
        <div className="prices-container">
          <h4>PRICE:</h4>
          <div>
            <p className="prev-price"></p>
            <p className="price">{price}$</p>
          </div>
        </div>
        <p className="game-details-desc">{description}</p>
        <div className="get-btn-container">
          {/* <div className="list-item-bg">
            <div className="tint-color"></div>
            <div className="secondary-color"></div>
          </div>
          <button>Get</button>
        </div> */}
          <BackgroundAnimation>
            <button>GET</button>
          </BackgroundAnimation>
        </div>
      </div>
      <div className="comment-section">
        <h2>Comments</h2>
        {gameComments.map(({ id, trainee, body }) => (
          <Comment userName={trainee || 'Anonimous'} comment={body} key={id} />
        ))}
        {authData.isAuthed ? (
          <>
            <h3>Post a comment</h3>
            <form onSubmit={onSubmit}>
              <input ref={inputRef} type="text" />
              <button type="submit">Comment</button>
            </form>
          </>
        ) : (
          <h3>Login to post comments about this game</h3>
        )}
      </div>
    </div>
  );
};

export default GameDetails;
