export const HOME_PAGE = 'HOME';
export const GAMES_LIST = 'GAMES LIST';
export const GAME_DETAILS = 'GAME DETAILS';
export const LOGIN_PAGE = 'LOGIN PAGE';

const BASE_URL = 'https://trainee-gamerbox.herokuapp.com/';

export const LIMIT = 5;

export const GAMES_URL = BASE_URL + `games?_start=:start&_limit=${LIMIT}`;
export const FEATURED_GAMES = BASE_URL + 'games?_start=0&_limit=7';
export const LOGIN_API_URL = BASE_URL + 'auth/local';
export const POST_COMMENT_URL = BASE_URL + 'games/:gameId/comment';
