export default class Connection {
  constructor() {
    if (!Connection.instance) {
      Connection.instance = {
        async getData(url) {
          try {
            const res = await fetch(url);
            return { resBody: await res.json(), error: res.status !== 200 };
          } catch (e) {
            return { error: true };
          }
        },
        async postData(url, body, headers) {
          try {
            const res = await fetch(url, {
              method: 'POST',
              headers: {
                'Content-Type': 'application/json',
                ...headers,
              },
              body: JSON.stringify(body),
            });
            return {
              resBody: await res.json(),
              error: res.status < 200 || res.status > 201,
            };
          } catch (error) {
            return { error: true };
          }
        },
        async patchData(url, body) {
          try {
            const res = await fetch(url, {
              method: 'PATCH',
              headers: {
                'Content-Type': 'application/json',
              },
              body: JSON.stringify(body),
            });
            return { error: res.status !== 200, resBody: await res.json() };
          } catch (error) {
            return { error: true };
          }
        },
        async deleteData(url) {
          try {
            const res = await fetch(url, {
              method: 'DELETE',
            });
            return { error: res.status !== 200, resBody: await res.json() };
          } catch (error) {
            return { error: false };
          }
        },
      };
    }
    return Connection.instance;
  }
}
